@extends('frontend.layouts.master')
@section('title', 'ห้องอบรม')
@section('content')
    <div id="main">
        <div class="wrapper">
            <section id="content">
                <div id="breadcrumbs-wrapper" class="grey lighten-3">
                    <div class="container">
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <h5 class="breadcrumbs-title">@yield('title')</h5>
                                <ol class="breadcrumbs">
                                    <li><a href="/">{{ config('app.name') }}</a></li>
                                    <li class="active">@yield('title')</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 112">
                            <table class="responsive-table">
                                <thead>
                                <tr>
                                    <th>ชื่อห้อง</th>
                                    <th>ประเภทห้อง</th>
                                    <th>อาคาร</th>
                                    <th>รายละเอียด</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($rooms as $room)
                                    <tr>
                                        <td>{{$room->name}}</td>
                                        <td>{{$room->type}}</td>
                                        <td>{{$room->building}}</td>
                                        <td><a href="/room/{{$room->id}}">ดูรายละเอียด</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
